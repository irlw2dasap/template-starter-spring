CREATE DATABASE IF NOT EXISTS project_app;

USE project_app;

CREATE TABLE `project_app`.`project_mdm_user` (
  `user_id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(32) NOT NULL,
  `password` VARCHAR(72) NOT NULL,
  `first_name` VARCHAR(45) NULL,
  `last_name` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `role` VARCHAR(16) NOT NULL DEFAULT "role_user",
  `created_at` DATETIME NOT NULL,
  `created_by` VARCHAR(32) NOT NULL,
  `updated_at` DATETIME NULL,
  `updated_by` VARCHAR(32) NULL,
  `use_status` CHAR(1) NOT NULL DEFAULT 'A',
  `record_status` CHAR(1) NOT NULL DEFAULT 'A',
PRIMARY KEY (`user_id`));

 CREATE TABLE `project_app`.`project_txn_refresh_token` (
  `refresh_id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `refresh_token` VARCHAR(255) NULL,
  `expired_at` DATETIME NOT NULL,
  `user_id` BIGINT(20) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `created_by` VARCHAR(32) NOT NULL,
  `updated_at` DATETIME NULL,
  `updated_by` VARCHAR(32) NULL,
  `use_status` CHAR(1) NOT NULL DEFAULT 'A',
  `record_status` CHAR(1) NOT NULL DEFAULT 'A',
PRIMARY KEY (`refresh_id`));
