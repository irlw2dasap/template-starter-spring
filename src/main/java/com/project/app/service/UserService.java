package com.project.app.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.project.app.entity.UserEntities;
import com.project.app.entity.specification.UserSpecification;
import com.project.app.model.response.HttpResponse;
import com.project.app.model.response.Pagination;
import com.project.app.repository.UserRepository;

@Service
public class UserService {
    
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserSpecification userSpecification;

    @Autowired
    private AuthService authService;

    public boolean isExists(String userName)
    {
        Optional<UserEntities> user = userRepository.findByUsername(userName);
        return user != null && user.isPresent();
    }


    public UserEntities create(UserEntities entities, String createdBy)
    {
        if (this.isExists(entities.getUsername()))
        {
            return null;
        }

        entities.setRecordStatus('A');
        entities.setUseStatus('A');
        entities.setCreatedAt(new Date());
        entities.setCreatedBy(createdBy);

        UserEntities savedEntities = userRepository.save(entities);
        return savedEntities;
    }

    public List<UserEntities> getList()
    {
        return userRepository.findAll();
    }

    public HttpResponse<List<UserEntities>> search(HttpServletRequest request, String q, int page, int limit, String sort, String order, String filterJsonString)
    {
        HttpResponse<List<UserEntities>> response = new HttpResponse<>();

        Sort.Direction direction = Sort.Direction.ASC;
        if (order.equalsIgnoreCase("desc")) {
            direction = Sort.Direction.DESC;
        }

        Pageable pageable = PageRequest.of(page - 1, limit, direction, sort);
        Specification specification = userSpecification.search(q, filterJsonString, null);

        Page<UserEntities> userPage = userRepository.findAll(specification, pageable);
        List<UserEntities> userList = userPage.getContent();

        for (UserEntities entity : userList) {
            Date lastestLogin = authService.getLastestLogin(entity);
            entity.setLastLogin(lastestLogin);
        }

        int length = (int) userPage.getTotalElements();
        int lastPage = (length / limit) + (length % limit == 0 ? 0 : 1);
        int startIndex = (page - 1) * limit;
        int endIndex = (page * limit) - 1;

        Pagination pagination = new Pagination();
        pagination.setLength(length);
        pagination.setSize(limit);
        pagination.setPage(page - 1);
        pagination.setLastPage(lastPage);
        pagination.setStartIndex(startIndex);
        pagination.setEndIndex(endIndex);

        response.setData(userList);
        response.setPagination(pagination);
        response.setMessage("ค้นหาข้อมูลสำเร็จ");
        response.setStatus("success");

        return response;
    }
}
