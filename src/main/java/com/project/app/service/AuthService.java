package com.project.app.service;

import java.time.Instant;
import java.util.Date;
import java.util.Optional;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.project.app.entity.RefreshTokenEntities;
import com.project.app.entity.UserEntities;
import com.project.app.model.request.LoginRequest;
import com.project.app.model.request.RefreshTokenRequest;
import com.project.app.model.request.TokenSignInRequest;
import com.project.app.model.response.HttpResponse;
import com.project.app.model.response.LoginResponse;
import com.project.app.repository.RefreshTokenRepository;
import com.project.app.repository.UserRepository;
import com.project.app.utils.JwtUtils;

import io.jsonwebtoken.Claims;

@Service
public class AuthService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private RefreshTokenRepository refreshTokenRepository;
    
    public ResponseEntity<?> login(LoginRequest data)
    {
        HttpResponse<LoginResponse> response = new HttpResponse<>();

        try {
            
            Optional<UserEntities> user = userRepository.findByUsername(data.getUsername());

            if (!user.isPresent() || !BCrypt.checkpw(data.getPassword(), user.get().getPassword())) {
                response.setStatus("fail");
                response.setMessage("ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง");
                return ResponseEntity.badRequest().body(response);
            }

            Optional<RefreshTokenEntities> existingRefreshToken = refreshTokenRepository.findByUserId(user.get().getUserId());
            if (existingRefreshToken.isPresent()) {
                // ถ้ามี Refresh Token เดิมอยู่ ให้ลบออกจากฐานข้อมูล
                refreshTokenRepository.delete(existingRefreshToken.get());
            }
        
            // สร้าง Access Token
            String accessToken = JwtUtils.generateAccessToken(user.get().getUsername());
        
            // สร้าง Refresh Token
            String refreshToken = JwtUtils.generateRefreshToken(user.get().getUsername());
        
            // สร้าง Refresh Token Entity
            RefreshTokenEntities refreshTokenEntity = new RefreshTokenEntities();
            refreshTokenEntity.setRefreshToken(refreshToken);
            refreshTokenEntity.setUserId(user.get().getUserId());
            refreshTokenEntity.setExpiredAt(Instant.now().plusMillis(JwtUtils.getRefreshTokenExpiration()));
            refreshTokenEntity.setUseStatus('A');
            refreshTokenEntity.setRecordStatus('A');
            refreshTokenEntity.setCreatedAt(new Date());
            refreshTokenEntity.setCreatedBy(user.get().getUsername());
            refreshTokenEntity.setUpdatedAt(new Date());
            refreshTokenEntity.setUpdatedBy(user.get().getUsername());
    
            // บันทึก Refresh Token ใหม่ในฐานข้อมูล
            refreshTokenRepository.save(refreshTokenEntity);
        
            LoginResponse loginResponse = new LoginResponse();
            loginResponse.setAccessToken(accessToken);
            loginResponse.setRefreshToken(refreshToken);
            loginResponse.setUser(user.get());

            response.setData(loginResponse);
            response.setMessage("เข้าสู่ระบบสำเร็จ!!");
            response.setStatus("success");
            
            return ResponseEntity.ok(response);

        } catch (Exception e) {

            e.printStackTrace();

            response.setMessage("เกิดข้อผิดพลาดในการเข้าสู่ระบบ");
            response.setStatus("error");

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);

        }
    }

    public ResponseEntity<?> refreshToken(RefreshTokenRequest refreshTokenRequest)
    {
        HttpResponse<LoginResponse> response = new HttpResponse<>();
        try {
            
            String refreshToken = refreshTokenRequest.getRefreshToken();

            if (refreshToken == null || refreshToken.isEmpty() || !JwtUtils.isValidToken(refreshToken)) {
                response.setStatus("fail");
                response.setMessage("การยืนยันตัวตนไม่ถูกต้อง");
                return ResponseEntity.badRequest().body(response);
            }
            
            // ตรวจสอบว่า Refresh Token อยู่ในสถานะที่สามารถรีเฟรชได้หรือไม่
            if (!tokenService.isValidRefreshToken(refreshToken)) {
                response.setStatus("fail");
                response.setMessage("ไม่พบข้อมูลการยืนยันตัวตน");
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body(response);
            }

            Claims claims = JwtUtils.decodeToken(refreshToken);
            String subject = claims.getSubject();
            
            Optional<UserEntities> user = userRepository.findByUsername(subject);

            if (!user.isPresent()) {
                response.setStatus("fail");
                response.setMessage("ไม่พบข้อมูลผู้ใช้");
                return ResponseEntity.badRequest().body(response);
            }

            // สร้าง Access Token ใหม่
            String accessToken = JwtUtils.generateAccessToken(user.get().getUsername());
            
            // ส่งข้อมูล Access Token ใหม่กลับไปยังผู้ใช้
            LoginResponse loginResponse = new LoginResponse();
            loginResponse.setAccessToken(accessToken);
            loginResponse.setRefreshToken(refreshToken);
            loginResponse.setUser(user.get());
            

            response.setData(loginResponse);
            response.setMessage("Refresh Token สำเร็จ!!");
            response.setStatus("success");

            return ResponseEntity.ok(response);
            
        } catch (Exception e) {
            e.printStackTrace();

            response.setMessage("เกิดข้อผิดพลาดในการตรวจสอบตัวตน");
            response.setStatus("error");

            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
        }
    }

    public ResponseEntity<?> signInWithToken(TokenSignInRequest tokenSignInRequest)
    {
        HttpResponse<LoginResponse> response = new HttpResponse<>();
        try {
            String accessToken = tokenSignInRequest.getAccessToken();

            if (!JwtUtils.isValidToken(accessToken)) {
                response.setStatus("fail");
                response.setMessage("การยืนยันตัวตนไม่ถูกต้อง");
                return ResponseEntity.badRequest().body(response);
            }

            Claims claims = JwtUtils.decodeToken(accessToken);
            String subject = claims.getSubject();
            
            Optional<UserEntities> user = userRepository.findByUsername(subject);
            if (!user.isPresent()) {
                response.setStatus("fail");
                response.setMessage("ไม่พบข้อมูลผู้ใช้");
                return ResponseEntity.badRequest().body(response);
            }

            Optional<RefreshTokenEntities> existingRefreshToken = refreshTokenRepository.findByUserId(user.get().getUserId());
            if (!existingRefreshToken.isPresent()) {
                response.setStatus("fail");
                response.setMessage("ไม่พบข้อมูลการยืนยันตัวตน");
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body(response);
            }

            String refreshToken = existingRefreshToken.get().getRefreshToken();
            if (refreshToken == null || refreshToken.isEmpty() || !tokenService.isValidRefreshToken(refreshToken)) {
                response.setStatus("fail");
                response.setMessage("ไม่พบข้อมูลการยืนยันตัวตน");
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body(response);
            }

            LoginResponse loginResponse = new LoginResponse();
            loginResponse.setAccessToken(accessToken);
            loginResponse.setRefreshToken(refreshToken);
            loginResponse.setUser(user.get());

            response.setData(loginResponse);
            response.setMessage("ยืนยันตัวตนด้วย Token สำเร็จ!!");
            response.setStatus("success");

            return ResponseEntity.ok(response);
            
        } catch (Exception e) {
            e.printStackTrace();

            response.setMessage("เกิดข้อผิดพลาดในการตรวจสอบตัวตน");
            response.setStatus("error");

            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
        }
    }

    public ResponseEntity<?> logout(String refreshToken)
    {
        HttpResponse<Boolean> response = new HttpResponse<>();

        if (!JwtUtils.isValidToken(refreshToken)) {
            response.setData(false);
            response.setMessage("การยืนยันตัวตนไม่ถูกต้อง");
            response.setStatus("fail");
            return ResponseEntity.ok().body(response);
        }

        Optional<RefreshTokenEntities> refreshTokenEntity = refreshTokenRepository.findByRefreshToken(refreshToken);
        if (!refreshTokenEntity.isPresent()) {
            response.setData(false);
            response.setMessage("การยืนยันตัวตนไม่ถูกต้อง");
            response.setStatus("fail");
            return ResponseEntity.ok().body(response);
        }

        refreshTokenEntity.get().setRecordStatus('I');
        
        refreshTokenRepository.save(refreshTokenEntity.get());

        response.setData(true);
        response.setMessage("ออกจากระบบสำเร็จ!!");
        response.setStatus("success");

        return ResponseEntity.ok(response);
    }

    public Date getLastestLogin(UserEntities entities) {
        Optional<RefreshTokenEntities> refreshTokenEntity = refreshTokenRepository.findLastest(entities.getUserId());
        if (!refreshTokenEntity.isPresent()) {
            return null;
        }
        return refreshTokenEntity.get().getCreatedAt();
    }
}
