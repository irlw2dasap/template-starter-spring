package com.project.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.app.entity.RefreshTokenEntities;
import com.project.app.repository.RefreshTokenRepository;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;

@Service
public class TokenService {

    @Autowired
    private RefreshTokenRepository refreshTokenRepository;

    public boolean isValidRefreshToken(String token) {
        try {
            Optional<RefreshTokenEntities> refreshTokenEntity = refreshTokenRepository.findByRefreshToken(token);
            if (refreshTokenEntity.isPresent() && refreshTokenEntity.get().getUseStatus() == 'A') {
                return true;
            }
        } catch (ExpiredJwtException | SignatureException e) {
            return false;
        }

        return false;
    }
}
