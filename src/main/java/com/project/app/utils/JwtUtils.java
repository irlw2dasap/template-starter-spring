package com.project.app.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;

import java.util.Date;

public class JwtUtils {
    
    private static final String SECRET_KEY = "betimes-secret-key";

    private static final long ACCESS_TOKEN_EXPIRATION = 30 * 60 * 1000; // 30 minutes
    private static final long REFRESH_TOKEN_EXPIRATION = 24 * 60 * 60 * 1000; // 24 hours

    public static String generateAccessToken(String subject) {
        Date now = new Date();
        Date expirationDate = new Date(now.getTime() + ACCESS_TOKEN_EXPIRATION);

        return Jwts.builder()
                .setSubject(subject)
                .setIssuedAt(now)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
                .compact();
    }

    public static String generateRefreshToken(String subject) {
        Date now = new Date();
        Date expirationDate = new Date(now.getTime() + REFRESH_TOKEN_EXPIRATION);

        return Jwts.builder()
                .setSubject(subject)
                .setIssuedAt(now)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
                .compact();
    }

    public static Claims decodeToken(String token) {
        return Jwts.parser()
                .setSigningKey(SECRET_KEY)
                .parseClaimsJws(token)
                .getBody();
    }

    public static boolean isValidToken(String token) {
        try {
            decodeToken(token);
            return true;
        } catch (SignatureException e) {
            return false;
        }
    }

    public static long getAccessTokenExpiration() {
        return ACCESS_TOKEN_EXPIRATION;
    }

    public static long getRefreshTokenExpiration() {
        return REFRESH_TOKEN_EXPIRATION;
    }
    
}
