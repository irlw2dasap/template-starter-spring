package com.project.app.model;

import lombok.Data;

@Data
public class PagingJson {
    private String next;
}
