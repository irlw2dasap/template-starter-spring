package com.project.app.model.request;

import lombok.Data;

@Data
public class LoginRequest {
    public String username;
    public String password;
}
