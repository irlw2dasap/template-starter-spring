package com.project.app.model.request;

import lombok.Data;

@Data
public class UserSignUpRequest {
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private String email;
}