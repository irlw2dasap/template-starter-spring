package com.project.app.model.request;

import lombok.Data;

@Data
public class TokenSignInRequest {
    private String accessToken;
}
