package com.project.app.model.response;

import lombok.Data;

@Data
public class HttpResponse<T> {
    private String status;
    private String message;
    private T data;
    private Pagination pagination;
}
