package com.project.app.model.response;

import com.project.app.entity.UserEntities;

import lombok.Data;

@Data
public class LoginResponse {
    public String accessToken;
    public String refreshToken;
    public UserEntities user;
}
