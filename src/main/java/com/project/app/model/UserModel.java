package com.project.app.model;

import java.util.Date;

import lombok.Data;

@Data
public class UserModel {
    private Long userId;
    private String username;
    private String firstName;
    private String lastName;
    private String role;
    private String email;
    private Date createdAt;
    private String createdBy;
    private Date updatedAt;
    private String updatedBy;
    private char useStatus;
    private char recordStatus;
}
