package com.project.app.model;

import lombok.Data;

@Data
public class ResponseJson {
    private Object data;
    private PagingJson paging;    
}
