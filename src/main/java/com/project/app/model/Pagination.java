package com.project.app.model;

import lombok.Data;

@Data
public class Pagination {
    private long length;
    private int size;
    private int page;
    private int lastPage;
    private int startIndex;
    private int endIndex;
}
