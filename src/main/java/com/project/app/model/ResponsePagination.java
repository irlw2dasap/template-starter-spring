package com.project.app.model;

import lombok.Data;

@Data
public class ResponsePagination<T> {
    private T data;
    private Pagination pagination;
}
