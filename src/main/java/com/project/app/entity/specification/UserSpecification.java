package com.project.app.entity.specification;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.project.app.entity.UserEntities;

@Component
public class UserSpecification {

    public Specification<UserEntities> byRecordStatus() {
        return (root, query, cb) -> cb.equal(
                root.get("recordStatus"), 'A');
    }

    public Specification<UserEntities> byFieldSearch(String q) {
        String likePattern = "%" + q + "%";
        return (root, query, cb) -> {
            System.out.println("q: " + q + " " + likePattern);
            return cb.or(
                cb.like(root.get("username"), likePattern),
                cb.like(root.get("firstName"), likePattern),
                cb.like(root.get("lastName"), likePattern),
                cb.like(root.get("email"), likePattern),
                cb.like(root.get("createdBy"), likePattern)
            );
        };
    }

    public Specification<UserEntities> search(String q, String filter, String username) {
        Specification<UserEntities> specification = Specification.where(byRecordStatus());

        if (q != null && !q.isEmpty()) {
            specification = specification.and(byFieldSearch(q));
        }

        if (filter != null && !filter.isEmpty()) {
            JSONArray additionalFilters = new JSONArray(filter);
            for (int i = 0; i < additionalFilters.length(); i++) {
                JSONObject filterObject = additionalFilters.getJSONObject(i);

                if (filterObject.has("key") && filterObject.has("value")) {
                    String field = filterObject.getString("key");
                    Object value = filterObject.get("value");

                    if (value != null && !value.toString().isEmpty()) {
                        specification = specification.and((root, query, cb) -> cb.equal(root.get(field), value));
                        System.out.println(field + " " + value);
                    }
                }
            }
        }

        if (username != null) {
            specification = specification.and((root, query, cb) -> cb.equal(root.get("createdBy"), username));
        }

        return specification;
    }
}