package com.project.app.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.project.app.entity.UserEntities;
import com.project.app.repository.UserRepository;
import com.project.app.utils.JwtUtils;

import io.jsonwebtoken.Claims;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class JwtFilter extends OncePerRequestFilter {
    
    private static final String HEADER_AUTHORIZATION = "Authorization";
    private static final String TOKEN_PREFIX = "Bearer ";

    @Autowired
    private UserRepository userRepository;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
                                        
        String authorizationHeader = request.getHeader(HEADER_AUTHORIZATION);

        if (authorizationHeader != null && authorizationHeader.startsWith(TOKEN_PREFIX)) {

            String token = authorizationHeader.substring(TOKEN_PREFIX.length());
            Claims claims = JwtUtils.decodeToken(token);
            String subject = claims.getSubject();

            if (claims != null) {
                Optional<UserEntities> user = userRepository.findByUsername(subject);
                if (user.isPresent()) {
                    request.setAttribute("username", user.get().getUsername());
                }
            }
        }

        filterChain.doFilter(request, response);
    }
}
