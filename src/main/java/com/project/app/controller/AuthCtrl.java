package com.project.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.project.app.model.request.LoginRequest;
import com.project.app.model.request.RefreshTokenRequest;
import com.project.app.model.request.TokenSignInRequest;
import com.project.app.service.AuthService;

@RestController
@CrossOrigin(origins = "*") 
@RequestMapping("/auth")
public class AuthCtrl {

    @Autowired
    private AuthService authService;

    @RequestMapping(value = "/sign-in", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public ResponseEntity<?> login(@RequestBody LoginRequest loginRequest) {
        return authService.login(loginRequest);
    }

    @RequestMapping(value = "/refresh-token", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public ResponseEntity<?> refreshToken(@RequestBody RefreshTokenRequest refreshTokenRequest) {
        try {
            return authService.refreshToken(refreshTokenRequest);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
        }
    }

    @RequestMapping(value = "/sign-in-with-token", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public ResponseEntity<?> signInWithToken(@RequestBody TokenSignInRequest tokenSignInRequest) {
        return authService.signInWithToken(tokenSignInRequest);
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public ResponseEntity<?> logout(@RequestBody RefreshTokenRequest refreshTokenRequest) {
        String refreshToken = refreshTokenRequest.getRefreshToken();
        return authService.logout(refreshToken);
    }

}
