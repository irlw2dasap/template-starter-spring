package com.project.app.controller;

import javax.servlet.http.HttpServletRequest;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.project.app.entity.UserEntities;
import com.project.app.model.request.UserSignUpRequest;
import com.project.app.model.response.HttpResponse;
import com.project.app.service.UserService;

@RestController
@CrossOrigin(origins = "*") 
@RequestMapping("/user")
public class UserCtrl {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(this.userService.getList());
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public ResponseEntity<?> getSearch(
        @RequestParam(required = false) String q,
        @RequestParam(defaultValue = "1", required = false) int page,
        @RequestParam(defaultValue = "10", required = false) int size,
        @RequestParam(defaultValue = "createdAt", required = false) String sort,
        @RequestParam(defaultValue = "desc", required = false) String order,
        @RequestParam(defaultValue = "[]", required = false) String filter,
        HttpServletRequest request
    ) {
        return ResponseEntity.ok(this.userService.search(request, q, page, size, sort, order, filter));
    }

    @RequestMapping(value = "/sign-up", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public ResponseEntity<?> userRegister(@RequestBody UserSignUpRequest bodyRequest) {

        HttpResponse<UserEntities> httpResponse = new HttpResponse<>();

        try {

            if (userService.isExists(bodyRequest.getUsername()))
            {
                httpResponse.setMessage("ชื่อผู้ใช้นี้ได้ถูกใช้แล้ว");
                httpResponse.setStatus("fail");
                return ResponseEntity.status(HttpStatus.CONFLICT).body(httpResponse);
            }

            String hashPassword = BCrypt.hashpw(bodyRequest.getPassword(), BCrypt.gensalt(12)); 
            
            UserEntities entities = UserEntities.builder()
                .username(bodyRequest.getUsername())
                .firstName(bodyRequest.getFirstName())
                .lastName(bodyRequest.getLastName())
                .email(bodyRequest.getEmail())
                .password(hashPassword)
                .role("role_user")
                .build();

            UserEntities savedEntities = userService.create(entities, "SYSTEM");

            if (savedEntities == null)
            {
                httpResponse.setMessage("ไม่สามารถสร้างผู้ใช้งานใหม่ได้");
                httpResponse.setStatus("error");
                return ResponseEntity.status(HttpStatus.CONFLICT).body(httpResponse);
            }

            return ResponseEntity.status(HttpStatus.CREATED).body(savedEntities);

        } catch (Exception e) {
            e.printStackTrace();
        }

        httpResponse.setMessage("เกิดข้อผิดพลาดในการสมัครสมาชิก");
        httpResponse.setStatus("error");
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(httpResponse);
    }

}
