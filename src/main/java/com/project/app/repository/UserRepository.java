package com.project.app.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.project.app.entity.UserEntities;

public interface UserRepository extends JpaRepository<UserEntities, Long> {
    @Override
    @Query(value = "SELECT m FROM UserEntities m WHERE recordStatus = 'A'")
    List<UserEntities> findAll();
    Page<UserEntities> findAll(Specification<UserEntities> specification, Pageable pageable);


    @Query(value = "SELECT u FROM UserEntities u WHERE username = :username AND recordStatus = 'A'")
    Optional<UserEntities> findByUsername(@Param("username") String username);
}
