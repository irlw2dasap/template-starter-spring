package com.project.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.project.app.entity.RefreshTokenEntities;

import java.util.Optional;

public interface RefreshTokenRepository extends JpaRepository<RefreshTokenEntities, Long>, JpaSpecificationExecutor<RefreshTokenEntities> {
    @Query(value = "SELECT e FROM RefreshTokenEntities e WHERE recordStatus = 'A' AND refreshToken = :token")
    Optional<RefreshTokenEntities> findByRefreshToken(@Param("token") String token);

    @Query(value = "SELECT e FROM RefreshTokenEntities e WHERE recordStatus = 'A' AND userId = :user_id")
    Optional<RefreshTokenEntities> findByUserId(@Param("user_id") Long userId);

    @Query(value = "SELECT e FROM RefreshTokenEntities e WHERE recordStatus = 'A' AND userId = :user_id ORDER BY id DESC")
    Optional<RefreshTokenEntities> findLastest(@Param("user_id") Long userId);
}
